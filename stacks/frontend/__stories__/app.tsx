import * as React from 'react';

import { storiesOf } from '@storybook/react';
import App from '../src/app';

storiesOf('App', module)
  .add('Some', () => (
    <App/>
  ));
