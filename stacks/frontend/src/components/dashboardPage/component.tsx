require('./styles.scss');

import * as React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';

import { InitialisedPeriodical } from '../../../../../lib/interfaces/Periodical';
import { InitialisedScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { getDashboard } from '../../services/periodical';
import { ArticleList } from '../articleList/component';
import { PageMetadata } from '../pageMetadata/component';
import { PeriodicalList } from '../periodicalList/component';
import { Spinner } from '../spinner/component';

interface DashboardData {
  articles: null | InitialisedScholarlyArticle[];
  periodicals: null | InitialisedPeriodical[];
}

interface DashboardPageState {
  dashboardData?: null | DashboardData;
}

export class DashboardPage
  extends React.Component<
  RouteComponentProps<{}>,
  DashboardPageState
> {
  public state: DashboardPageState = {};

  constructor(props: any) {
    super(props);
  }

  public async componentDidMount() {
    try {
      const response = await getDashboard();

      this.setState({ dashboardData: response });
    } catch (e) {
      this.setState({ dashboardData: null });
    }
  }

  public render() {
    if (typeof this.state.dashboardData === 'undefined') {
      return (
        <div className="container">
          <Spinner/>
        </div>
      );
    }
    if (this.state.dashboardData === null) {
      return (<div className="callout alert">There was a problem loading your dashboard.</div>);
    }

    return this.renderDashboard(this.state.dashboardData);
  }

  private renderDashboard(data: DashboardData) {
    return (
      <div>
        <section className="hero is-medium is-dark is-bold">
          <div className="hero-body">
            <div className="container">
              <PageMetadata
                url={this.props.match.url}
                title="Dashboard"
                description="Manage your journals and articles"
              />
              <h1 className="title">Dashboard</h1>
            </div>
          </div>
        </section>
        {this.renderSuggestion()}
        {this.renderArticleList(data.articles)}
        {this.renderPeriodicalList(data.periodicals)}
      </div>
    );
  }

  private renderArticleList(articles?: InitialisedScholarlyArticle[] | null) {
    if (typeof articles === 'undefined' || articles === null) {
      return null;
    }

    if (articles.length === 0) {
      return (
        <section className="section" data-test-id="article-list">
          <div className="container">
            <div className="columns">
              <div className="column is-half-widescreen">
                <h2 className="title is-size-4">Your articles</h2>
                <p>
                  You have not published any articles on Flockademic. Would you like to&nbsp;
                  <Link
                    to="/articles/new"
                    title="Submit a new manuscript"
                  >
                    submit one now
                  </Link>
                  ?
                </p>
              </div>
            </div>
          </div>
        </section>
      );
    }

    return (
      <section className="section" data-test-id="article-list">
        <div className="container">
          <div className="columns">
            <div className="column is-half-widescreen">
              <h2 className="title is-size-4">Your articles</h2>
              <ArticleList articles={articles}/>
            </div>
          </div>
        </div>
      </section>
    );
  }

  private renderPeriodicalList(periodicals?: InitialisedPeriodical[] | null) {
    if (typeof periodicals === 'undefined' || periodicals === null) {
      return null;
    }

    if (periodicals.length === 0) {
      return (
        <section className="section" data-test-id="journal-list">
          <div className="container">
            <div className="columns">
              <div className="column is-half-widescreen">
                <h2 className="title is-size-4">Preprint journals you manage</h2>
                <p>
                  You do not manage any preprint journals. Would you like to&nbsp;
                  <Link
                    to="/journals/new"
                    title="Start a new preprint journal"
                  >
                    start one now
                  </Link>
                  ?
                </p>
              </div>
            </div>
          </div>
        </section>
      );
    }

    return (
      <section className="section" data-test-id="journal-list">
        <div className="container">
          <div className="columns">
            <div className="column is-half-widescreen">
              <h2 className="title is-size-4">Preprint journals you manage</h2>
              <PeriodicalList periodicals={periodicals}/>
            </div>
          </div>
        </div>
      </section>
    );
  }

  private renderSuggestion() {
    const suggestion = this.getSuggestion();

    /* istanbul ignore else: no suggestions implemented yet */
    if (suggestion === null) {
      return null;
    }

    /* istanbul ignore next: no suggestions implemented yet */
    return (
      <section className="section">
        <div className="container">
          {suggestion}
        </div>
      </section>
    );
  }

  private getSuggestion() {
    // Suggestion ideas:
    // - "Can't find what you're looking for? Sign in to your account."

    return null;
  }
}
